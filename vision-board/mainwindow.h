#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtGui/QMainWindow>
#include <QtGui/QStatusBar>

#include "ticker/tickerboard.h"


class MainWindow : public QWidget
{
    Q_OBJECT

public:

    TickerBoard *tickerBoard;

    QStatusBar *statusBar;

    MainWindow(QWidget *parent = 0);
    ~MainWindow();


public slots:
    void on_show_settings();
    void on_quit();
};

#endif // MAINWINDOW_H
