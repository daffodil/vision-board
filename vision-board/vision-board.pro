# -------------------------------------------------
# Project created by QtCreator 2011-10-06T14:37:16
# -------------------------------------------------
QT += network \
    sql
TARGET = vision-board
TEMPLATE = app
SOURCES += main.cpp \
    mainwindow.cpp \
    ticker/tickerboard.cpp \
    ticker/cellwidget.cpp \
    ticker/cellwidgetlabel.cpp \
    settings/settingsdialog.cpp
HEADERS += mainwindow.h \
    ticker/tickerboard.h \
    ticker/cellwidget.h \
    ticker/cellwidgetlabel.h \
    settings/settingsdialog.h
RESOURCES += resources/icons.qrc
