#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QtGui/QDialog>
#include <QtGui/QLineEdit>

class SettingsDialog : public QDialog
{
Q_OBJECT
public:
    explicit SettingsDialog(QWidget *parent = 0);

    QLineEdit *txtServerUrl;
    QLineEdit *txtEditDailyTarget;

signals:

public slots:

};

#endif // SETTINGSDIALOG_H
