#ifndef CELLWIDGETLABEL_H
#define CELLWIDGETLABEL_H

#include <QtCore/QString>

#include <QtGui/QWidget>
#include <QtGui/QLabel>

class CellWidgetLabel : public QLabel
{
Q_OBJECT
public:
    explicit CellWidgetLabel(QWidget *parent = 0);

    QString _value;
    QString _bg_color;
    QString _color;
    QString _border;

    void set_value(QString v);
    QString get_value();
    void set_border( QString border);
    void set_color( QString color);
    void set_background_color(QString color);
    void set_font_size(int size);
    void _set_style();


signals:

public slots:

};

#endif // CELLWIDGETLABEL_H
