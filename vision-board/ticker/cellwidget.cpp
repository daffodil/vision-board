
#include <QtGui/QVBoxLayout>


#include "cellwidget.h"

CellWidget::CellWidget(QWidget *parent) :
    QWidget(parent)
{

    setAutoFillBackground(false);

    QVBoxLayout *mainVBox = new QVBoxLayout();
    mainVBox->setContentsMargins(0,0,0,0);
    mainVBox->setSpacing(0);
    setLayout(mainVBox);

    topLabel = new CellWidgetLabel();
    set_label_font(topLabel, 14);
    topLabel->set_border("1px solid #dddddd");
    mainVBox->addWidget(topLabel);

    bottomLabel = new CellWidgetLabel();
    set_label_font(bottomLabel, 11);
    bottomLabel->set_border("3px solid #666666");
    mainVBox->addWidget(bottomLabel);

}


 void CellWidget::set_label_font(QLabel *label, int size)
{
    QFont f = label->font();
    f.setPointSize(size);
    f.setFamily("monospace");
    label->setFont(f);
}
 void CellWidget::set_background_color(QString str_color)
 {
    topLabel->set_background_color(str_color);
    bottomLabel->set_background_color(str_color);

}
