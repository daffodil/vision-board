#include <QDebug>

#include <QtSql/QSqlQuery>

#include <QtCore/QString>
#include <QtCore/QStringList>
#include <QtCore/QDate>
#include <QtCore/QDateTime>
#include <QtCore/QTimer>

#include <QtGui/QHBoxLayout>
#include <QtGui/QFont>

#include <QtGui/QHeaderView>

#include "ticker/tickerboard.h"
#include "ticker/cellwidget.h"

TickerBoard::TickerBoard(QWidget *parent) :
    QWidget(parent)
{

    int idx;

    QHBoxLayout *mainLayout = new QHBoxLayout();
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    setLayout(mainLayout);


    //== Table
    table = new QTableWidget(ROW_COUNT, 8);
    table->verticalHeader()->hide();
    mainLayout->addWidget(table);

    //== Headers
    QTableWidgetItem *item;
    item = new QTableWidgetItem("Week");
    table->setHorizontalHeaderItem( C_WEEK, item);
    table->setColumnWidth(C_WEEK, 150);

    QStringList top_labels;
    top_labels << "Mon" << "Tue" << "Wed" << "Thu" << "Fri";
    for(idx = 0; idx < top_labels.length(); idx++){
        item = new QTableWidgetItem(top_labels.at(idx));
        table->setHorizontalHeaderItem( idx + 1, item);
        table->setColumnWidth(idx + 1, 50);
    }

    item = new QTableWidgetItem("Total");
    table->setHorizontalHeaderItem(C_TOTAL, item);

    item = new QTableWidgetItem("Target %");
    table->setHorizontalHeaderItem(C_TARGET, item);

    //= Set font and alignment
    for(idx = 0; idx < table->columnCount(); idx++){
        QFont font = table->horizontalHeaderItem(idx)->font();
        font.setPointSize(11);
        table->horizontalHeaderItem(idx)->setFont(font);
        table->horizontalHeaderItem(idx)->setTextAlignment(Qt::AlignRight|Qt::AlignVCenter);
    }

    QTimer::singleShot(250, this, SLOT(init_table()));
}

void TickerBoard::init_table()
{
    //=== Init Rows
     CellWidget *wid;
     for(int row = 0; row < table->rowCount(); row++){

         //= weeks
         wid = new CellWidget();
         wid->topLabel->setText("-");
         wid->bottomLabel->setText("-");
         wid->set_background_color("#dddddd");
         wid->bottomLabel->set_font_size(8);
         table->setCellWidget(row, C_WEEK, wid);

         //= weekdays
         for(int col= C_MON; col < C_FRI + 1; col++){
             wid = new CellWidget();
             wid->topLabel->setText("-");
             wid->bottomLabel->setText("-");
             wid->set_background_color("#FEFFD4");
             table->setCellWidget(row, col, wid);
         }

         //= total
         wid = new CellWidget();
         wid->topLabel->setText("-");
         wid->bottomLabel->setText("-");
         wid->set_background_color("#F6F79B");
         table->setCellWidget(row, C_TOTAL, wid);

         //= target
         wid = new CellWidget();
         wid->topLabel->setText("-");
         wid->bottomLabel->setText("-");
         wid->set_background_color("#F7E19B");
         table->setCellWidget(row, C_TARGET, wid);


         /* ## Reps
         for c in range(0, REPS_COUNT):
                 #self.table.setColumnWidth(c, 150)
                 wid = CellWidget()
                 wid.topLabel.setText("-" )
                 wid.bottomLabel.setText("-")
                 wid.set_background_color("#efefef")
                 self.table.setCellWidget(r, self.COL_COUNT + c, wid) */

     } // for(row)
    /*
     QSqlQuery query;

     if(query.exec("select * from users")){
         qDebug() << "yes";
         while (query.next()) {
                  QString name = query.value(3).toString();
                  qDebug() << name;
              }
     }else{
         qDebug() << "NO";
     }
     */
}


void TickerBoard::set_table_dimensions()
{
    int row_height = table->height() / ROW_COUNT;
    for(int row =0; row < table->rowCount(); row++){
        table->setRowHeight(row, row_height);
    }
}

void TickerBoard::load_weeks()
{
    QDate today = QDate::currentDate();
    int dow = today.dayOfWeek();
    QDate monday = today.addDays((dow * -1) + 1);

    QDate friday = monday.addDays(4);
    qDebug() << dow << monday << friday;
    for(int row_idx=0 ; row_idx < table->rowCount(); row_idx++){
        qDebug() << row_idx;
        int idx = (row_idx * 7) * -1;
        QDate mon = monday.addDays(idx);
        QDate fri = monday.addDays(idx + 5);
        /*
        QTableWidgetItem *item = new QTableWidgetItem();
        item->setText(mon.toString("d MMM").append(" - ").append(fri.toString("d MMM")));
        table->setItem(idx, 0, item);
        */
        CellWidget *wid = new CellWidget();
        wid->topLabel->setText("-");
        wid->bottomLabel->setText(mon.toString("d MMM").append(" - ").append(fri.toString("d MMM")));
        wid->set_background_color("#dddddd");
        wid->bottomLabel->set_font_size(8);
        table->setCellWidget(row_idx, C_WEEK, wid);
        //self.set_week(r);

        //qDebug() << item;
    }
}


void TickerBoard::calc_now()
{
qDebug() << "cals now";

}
