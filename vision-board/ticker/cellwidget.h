#ifndef CELLWIDGET_H
#define CELLWIDGET_H

#include <QWidget>
#include "ticker/cellwidgetlabel.h"


class CellWidget : public QWidget
{
Q_OBJECT
public:
    explicit CellWidget(QWidget *parent = 0);

    CellWidgetLabel *topLabel;
    CellWidgetLabel *bottomLabel;

    void set_label_font(QLabel *label, int size);
    void set_background_color(QString str_color);
signals:

public slots:

};

#endif // CELLWIDGET_H
