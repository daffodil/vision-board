#include "cellwidgetlabel.h"

CellWidgetLabel::CellWidgetLabel(QWidget *parent) :
    QLabel(parent)
{

            _value = "";
            _bg_color = "white";
            _color = "black";
            _border = "1px solid transparent";

            setAlignment(Qt::AlignRight|Qt::AlignVCenter);
            _set_style();

}
void CellWidgetLabel::set_value(QString v)
{
            _value = v;
            setText(v); //core.utils.to_thousands(v))
}

QString CellWidgetLabel::get_value()
{
    return _value;
}
void CellWidgetLabel::set_border(QString border)
{
    _border = border;
    _set_style();
}
void CellWidgetLabel::set_color(QString color)
{
    _color = color;
    _set_style();
}
void CellWidgetLabel::set_background_color(QString color)
{
    _bg_color = color;
    _set_style();
}
void CellWidgetLabel::_set_style()
{
    setStyleSheet(  QString("padding-right: 5px; border-bottom: %1; border-right: 1px solid #dddddd; background-color: %2; color: %3"
                  ).arg(_border).arg(_bg_color).arg(_color)
                  );
                                                    //% (self._border, self._bg_color, self._color)
                                                    //)
}

void CellWidgetLabel::set_font_size(int size)
{
    QFont f = font();
    f.setPointSize(size);
    setFont(f);
}
