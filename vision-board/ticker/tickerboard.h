#ifndef TICKERBOARD_H
#define TICKERBOARD_H

#include <QtGui/QWidget>
#include <QtGui/QTableWidget>
#include <QtGui/QTableWidgetItem>


class TickerBoard : public QWidget
{
Q_OBJECT
public:
    explicit TickerBoard(QWidget *parent = 0);

    enum COLS{
        C_WEEK = 0,
        C_MON = 1,
        C_TUE = 2,
        C_WED = 3,
        C_THU = 4,
        C_FRI = 5,
        C_TOTAL = 6,
        C_TARGET = 7
    };
    static const int ROW_COUNT = 7;

    QTableWidget *table;

    void load_weeks();

signals:

public slots:
    void init_table();
    void set_table_dimensions();

    void calc_now();
};

#endif // TICKERBOARD_H
