
#include <QDebug>

#include <QtSql/QSqlDatabase>

#include <QtGui/QApplication>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QToolBar>
#include <QtGui/QAction>

#include "mainwindow.h"
#include "settings/settingsdialog.h"

MainWindow::MainWindow(QWidget *parent)
    : QWidget(parent)
{
    if(1 == 0){
        setWindowState(Qt::WindowMaximized);
    }else{
        setFixedHeight(800);
        setFixedWidth(800);
        move(50,50);
    }
    setWindowTitle("Vision Board");
    setWindowIcon(QIcon(":/icon/logo"));

    //= container
    QVBoxLayout *containerLayout = new QVBoxLayout();
    containerLayout->setContentsMargins(0,0,0,0);
    containerLayout->setSpacing(0);
    setLayout(containerLayout);

    //======================================================
    //== Top Toolbar
    QToolBar *toolbar = new QToolBar();
    toolbar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    containerLayout->addWidget(toolbar, 0);

    QAction *actQuit = new QAction(toolbar);
    actQuit->setIcon(QIcon(":/icon/quit"));
    actQuit->setText("Quit");
    toolbar->addAction(actQuit);
    connect(actQuit,  SIGNAL(triggered()), this, SLOT(on_quit()));

    toolbar->addSeparator();

    QAction *actSettings = new QAction(toolbar);
    actSettings->setIcon(QIcon(":/icon/settings"));
    actSettings->setText("Settings");
    toolbar->addAction(actSettings);
    connect(actSettings, SIGNAL(triggered()), this, SLOT(on_show_settings()));

    //= Ticker in middle
    tickerBoard = new TickerBoard();
    containerLayout->addWidget(tickerBoard, 200);


    //= StatusBar
    statusBar = new QStatusBar();
    containerLayout->addWidget(statusBar, 0);

    //SYSDBA:masterkey@192.168.5.33:3050/f:/Vision/data/WinAcc.gdb
    QSqlDatabase db = QSqlDatabase::addDatabase("QIBASE");
    db.setHostName("localhost");
    db.setDatabaseName("io");
    db.setUserName("root");
    db.setPassword("mash");
    bool ok = db.open();
    qDebug() << ok;

}

MainWindow::~MainWindow(){}


void MainWindow::on_show_settings()
{
    SettingsDialog *settingsDialog = new SettingsDialog();
    settingsDialog->exec();
}

void MainWindow::on_quit()
{
    QApplication::instance()->exit(0);
}

